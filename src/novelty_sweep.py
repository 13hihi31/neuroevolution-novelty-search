import time
import numpy as np
import torch
import torch.multiprocessing as mp
import signal
from utils import build_net, worker_func, novelty_func
from utils import make_mountain_car_env
import matplotlib.pyplot as plt

workers_count = 4
max_seed = 2**32 - 1
stop_fitness = 95
max_episodes = 6000

class Evaluate:
  def __init__(self, behavior_tail_size):
    self.behavior_tail_size = behavior_tail_size

  def __call__(self, env, net):
    obs = env.reset()
    reward = 0.0
    steps = 0
    behavior = []
    while True:
      obs_v = torch.FloatTensor([obs])
      action = net(obs_v)
      action = action.data.cpu().numpy()
      action = np.clip(action,
                       env.action_space.low[0],
                       env.action_space.high[0])[0].astype(np.float32)
      obs, r, done, _ = env.step(action)
      reward += r
      steps += 1
      behavior.append(obs)
      if done:
        break
    behavior = behavior[-self.behavior_tail_size:]
    behavior = np.append(np.array(behavior).reshape(-1), [reward])
    return behavior, steps
  
def make_env():
  return make_mountain_car_env()

def train(seeds_per_worker,
          parents_count,
          noise_std,
          behavior_tail_size,
          archive_size,
          nearest_neighbors,
          add_to_archive_prob):
  
  input_queues = []
  output_queue = mp.Queue(maxsize=workers_count)
  workers = []

  for _ in range(workers_count):
    input_queue = mp.Queue(maxsize=1)
    input_queues.append(input_queue)
    w = mp.Process(target=worker_func,
                   args=(input_queue, output_queue,
                         noise_std, make_env, Evaluate(behavior_tail_size)))
    w.start()
    workers.append(w)
    seeds = [(np.random.randint(max_seed),) for _ in range(seeds_per_worker)]
    input_queue.put(seeds)
  
  gen_idx = 1
  episodes_per_gen = seeds_per_worker * workers_count
  elite = None
  max_reward_mean = None
  behavior_archive = []
  seeds_archive = []
  best_fitness_score = None
  best_agent_seeds = None
  
  novelties_mean = []
  novelties_min = []
  novelties_max = []
  novelties_std = []
  num_episodes = []
  speed = []
  best_fitness_list = []
  
  while True:
    t_start = time.time()    
    batch_steps = 0
    population = []
    
    while len(population) < episodes_per_gen:
      out_item = output_queue.get()
      population.append((out_item.seeds, out_item.metric))    
      batch_steps += out_item.steps
      
    if elite is not None:
      population.append(elite)

    population_behaviors = np.array([p[1] for p in population])
    
    novelty_scores = []
    best_novelty = None
    best_behavior = None
    best_seed = None
    best_added_to_archive = False
    
    for k in range(len(population)):
      novelty_score_k = novelty_func(k, population_behaviors,
                                     behavior_archive, nearest_neighbors)
      novelty_scores.append(novelty_score_k)

      fitness_score = population[k][1][-1]
      if best_fitness_score is None or best_fitness_score < fitness_score:
        best_fitness_score = fitness_score
        best_agent_seeds = population[k][0]
      
      if best_novelty is None or best_novelty < novelty_score_k:
        best_novelty = novelty_score_k
        best_behavior = population_behaviors[k]
        best_seed = population[k][0]
        best_added_to_archive = False
        
      if np.random.rand() < add_to_archive_prob:
        behavior_archive.append(population_behaviors[k])
        seeds_archive.append(population[k][0])
        if best_novelty == novelty_score_k:
          best_added_to_archive = True
          
    if best_added_to_archive == False:
      behavior_archive.append(best_behavior)
      seeds_archive.append(best_seed)
          
    behavior_archive = behavior_archive[-archive_size:]
    seeds_archive = seeds_archive[-archive_size:]
    
    novelty_scores = np.array(novelty_scores)
    sorted_idxs = np.argsort(-novelty_scores)
    
    top_population = []
    for idx in sorted_idxs[:parents_count]:
      top_population.append(population[idx])
    elite = top_population[0]
        
    novelties = novelty_scores[sorted_idxs[:parents_count]]
    novelties_mean.append(np.mean(novelties))
    novelties_min.append(np.min(novelties))
    novelties_max.append(np.max(novelties))
    novelties_std.append(np.std(novelties))
    num_episodes.append(gen_idx * episodes_per_gen)
    speed.append(batch_steps / (time.time() - t_start))
    best_fitness_list.append(best_fitness_score)
    
    print('%d, %d: mean=%.2f, min=%.2f, max=%.2f, std=%.2f, speed=%.2f f/s, max_fitness=%.2f' %
          (gen_idx, gen_idx * episodes_per_gen, novelties_mean[-1], novelties_min[-1],
           novelties_max[-1], novelties_std[-1], speed[-1], best_fitness_score))

    if best_fitness_score > stop_fitness or num_episodes[-1] > max_episodes:
      break

    for worker_queue in input_queues:
      seeds = []
      for _ in range(seeds_per_worker):
        parent = np.random.randint(parents_count)
        next_seed = np.random.randint(max_seed)
        seeds.append(tuple(list(top_population[parent][0]) + [next_seed]))
      worker_queue.put(seeds)
    gen_idx += 1

  for worker_queue in input_queues:
    worker_queue.put(None)  
  for worker in workers:
    worker.join()

  return (novelties_mean, novelties_min, novelties_max,
          novelties_std, num_episodes, speed, best_fitness_list)

if __name__ == '__main__':
  mp.set_start_method('spawn')
  
  population_size = 400
  seeds_per_worker = population_size // workers_count
  parents_count = 40
  noise_std = 0.2
  behavior_tail_size = 40
  archive_size = 3000
  nearest_neighbors = 20
  add_to_archive_prob = 0.2

  population_size_l = [100, 500, 1000, 2000]
  seeds_per_worker_l = []
  for population_size_i in population_size_l:
    seeds_per_worker_l.append(population_size_i // workers_count)  
  parents_count_l = [5, 10, 50, 100]
  noise_std_l = [0.1, 0.2, 0.3, 0.4]
  nearest_neighbors_l = [10, 30, 90, 270]
  add_to_archive_prob_l = [0.2, 0.4, 0.6, 0.8]

  labels = []
  nov_mean_l = []
  nov_min_l = []
  nov_max_l = []
  nov_std_l = []
  num_ep_l = []
  speed_l = []
  best_fit_l = []
  
  #for idx, seeds_per_worker in enumerate(seeds_per_worker_l):
  #  labels.append(str(population_size_l[idx]))
  
  for parents_count in parents_count_l:
    labels.append(str(parents_count))
  
  #for noise_std in noise_std_l:
  #  labels.append(str(noise_std))

  #for nearest_neighbors in nearest_neighbors_l:
  #  labels.append(str(nearest_neighbors))
    
  #for add_to_archive_prob in add_to_archive_prob_l:
  #  labels.append(str(add_to_archive_prob))

    #np.random.seed(0)
    (nov_mean, nov_min,
     nov_max, nov_std,
     num_ep, speed,
     best_fit) = train(seeds_per_worker,
                       parents_count,
                       noise_std,
                       behavior_tail_size,
                       archive_size,
                       nearest_neighbors,
                       add_to_archive_prob)
    
    nov_mean_l.append(nov_mean)
    nov_min_l.append(nov_min)
    nov_max_l.append(nov_max)
    nov_std_l.append(nov_std)
    num_ep_l.append(num_ep)
    speed_l.append(speed)
    best_fit_l.append(best_fit)

  fig, axs = plt.subplots(6, 1, sharex=True)
  for idx in range(len(nov_mean_l)):
    axs[0].plot(nov_mean_l[idx], label=labels[idx])
    axs[0].set_ylabel("mean novelty")
    
    axs[1].plot(nov_min_l[idx], label=labels[idx])
    axs[1].set_ylabel("min novelty")
    
    axs[2].plot(nov_max_l[idx], label=labels[idx])
    axs[2].set_ylabel("max novelty")
    
    axs[3].plot(nov_std_l[idx], label=labels[idx])
    axs[3].set_ylabel("std novelty")
    
    axs[4].step(list(range(len(num_ep_l[idx]))), num_ep_l[idx], label=labels[idx])
    axs[4].set_ylabel("episodes")
    
    #axs[5].plot(speed_l[idx], label=labels[idx])
    #axs[5].set_ylabel("speed")

    axs[5].plot(best_fit_l[idx], label=labels[idx])
    axs[5].set_ylabel("best fitness")

  for idx in range(6):
    axs[idx].legend()

  axs[5].set_xlabel('generation number')
  
  plt.show()
  #fig.savefig("plots/population_size.pdf", bbox_inches='tight', dpi=fig.dpi)
  fig.savefig("plots/parents_count.pdf", bbox_inches='tight', dpi=fig.dpi)
  #fig.savefig("plots/noise_std.pdf", bbox_inches='tight', dpi=fig.dpi)
  #fig.savefig("plots/nearest_neighbors.pdf", bbox_inches='tight', dpi=fig.dpi)
  #fig.savefig("plots/add_to_archive_prob.pdf", bbox_inches='tight', dpi=fig.dpi)
  
