import collections
import copy
from model import Net
import numpy as np
import torch
from my_mountain_car import my_Continuous_MountainCarEnv
from my_pendulum import my_PendulumEnv
from gym.wrappers import TimeLimit

OutputItem = collections.namedtuple('OutputItem', field_names=['seeds', 'metric', 'steps'])
  
def mutate_net(net, seed, noise_std, copy_net=True):
  new_net = copy.deepcopy(net) if copy_net else net
  np.random.seed(seed)
  for p in new_net.parameters():
    noise_t = torch.tensor(np.random.normal(size=p.data.size()).astype(np.float32))
    p.data += noise_std * noise_t
  return new_net
  
def build_net(env, seeds, noise_std):
  torch.manual_seed(seeds[0])
  obs_size = env.observation_space.shape[0]
  act_size = env.action_space.shape[0]
  act_min = env.action_space.low[0]
  act_max = env.action_space.high[0]
  net = Net(obs_size, act_size, act_min, act_max)
  for seed in seeds[1:]:
    net = mutate_net(net, seed, noise_std, copy_net=False)
  return net
  
def make_pendulum_env(episode_steps=200):
  return TimeLimit(my_PendulumEnv(), max_episode_steps=episode_steps)
  
def make_mountain_car_env(episode_steps=800):
  return TimeLimit(my_Continuous_MountainCarEnv(), max_episode_steps=episode_steps)
  
def worker_func(input_queue, output_queue, noise_std, make_env, evaluate_fn):
  env = make_env()
  cache = {}
  while True:
    parents = input_queue.get()
    if parents is None:
      break
    new_cache = {}
    for net_seeds in parents:
      if len(net_seeds) > 1:
        net = cache.get(net_seeds[:-1])
        if net is not None:
          net = mutate_net(net, net_seeds[-1], noise_std)
        else:
          net = build_net(env, net_seeds, noise_std)
      else:
        net = build_net(env, net_seeds, noise_std)
      new_cache[net_seeds] = net
      metric, steps = evaluate_fn(env, net)
      output_queue.put(OutputItem(seeds=net_seeds, metric=metric, steps=steps))
    cache = new_cache

def novelty_func(behavior_idx, population_behaviors, behavior_archive, nearest_neighbors):
  behavior = population_behaviors[behavior_idx]
  dist_squared = np.sum((population_behaviors - behavior) ** 2, 1)
  if len(behavior_archive) > 0:
    dist_squared = np.append(dist_squared, np.sum((behavior_archive - behavior) ** 2, 1))
  dist_squared.sort()
  novelty = np.mean(dist_squared[1:nearest_neighbors+1])
  return novelty
  
