import torch
import torch.nn as nn

HID_SIZE = 40

class ScaledTanh(nn.Module):
  def __init__(self, min_val, max_val):
    super().__init__()
    self.val_diff = max_val - min_val
    self.val_sum = max_val + min_val
  def forward(self, x):
    return (torch.tanh(x) * self.val_diff + self.val_sum) / 2

class Net(nn.Module):
  def __init__(self, obs_size, act_size, min_act, max_act):
    super().__init__()
    self.net = nn.Sequential(
      nn.Linear(obs_size, HID_SIZE),
      nn.Tanh(),
      nn.Linear(HID_SIZE, HID_SIZE),
      nn.Tanh(),
      nn.Linear(HID_SIZE, act_size),
      ScaledTanh(min_act, max_act))

  def forward(self, x):
    return self.net(x)
    
