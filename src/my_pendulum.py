import numpy as np
from gym.envs.classic_control.pendulum import PendulumEnv

class my_PendulumEnv(PendulumEnv):
  def reset(self):
    self.state = np.array([np.pi, 0.0])
    self.last_u = None
    return self._get_obs()
