import numpy as np
from gym.envs.classic_control.continuous_mountain_car import Continuous_MountainCarEnv

class my_Continuous_MountainCarEnv(Continuous_MountainCarEnv):
  def reset(self):
    self.state = np.array([-0.5, 0])
    return np.array(self.state)
