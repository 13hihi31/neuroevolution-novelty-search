import torch
import time
from model import Net
import numpy as np
from utils import make_pendulum_env, make_mountain_car_env

FPS = 25

if __name__ == '__main__':
  env = make_pendulum_env()
  #env = make_mountain_car_env()
  
  obs_size = env.observation_space.shape[0]
  act_size = env.action_space.shape[0]
  act_min = env.action_space.low[0]
  act_max = env.action_space.high[0]
  
  net = Net(obs_size, act_size, act_min, act_max)
  net.load_state_dict(torch.load('best-agent.data'))
  #net.load_state_dict(torch.load('best-agent-novelty-pendulum.data'))
  #net.load_state_dict(torch.load('best-agent-novelty-mountain-car.data'))
  
  state = env.reset().astype(np.float32)
  total_reward = 0.0

  num_steps = 0
  
  while True:
    start_ts = time.time()
    env.render()
    
    obs_v = torch.FloatTensor([state])
    action = net(obs_v)
    action = action.data.cpu().numpy()[0]
    action = np.clip(action, act_min, act_max).astype(np.float32)
    
    # choose random action
    #action = env.action_space.sample()                
    #state, reward, done, _ = env.step([action])

    state, reward, done, _ = env.step(action)
    state = state.astype(np.float32)
    total_reward += reward
    
    num_steps += 1
    
    if done:
      print('number of steps:', num_steps)
      break
      
    delta = 1/FPS - (start_ts - time.time())
    if delta > 0:
      time.sleep(delta)
      
  env.close()
  print('Total reward %.2f' % total_reward)
  
