import time
import numpy as np
from tensorboardX import SummaryWriter
import torch
import torch.multiprocessing as mp
import signal
from utils import build_net, worker_func, novelty_func
from utils import make_pendulum_env, make_mountain_car_env

population_size = 400
workers_count = 4
parents_count = 20
seeds_per_worker = population_size // workers_count
noise_std = 0.3
max_seed = 2**32 - 1

behavior_sample_rate = 4
behavior_tail_size = 20
archive_size = 3000
nearest_neighbors = 20
add_to_archive_prob = 0.4
reward_weight = 0.05

def signal_handler(signal, frame):
  global interrupted
  interrupted = True

interrupted = False

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

def evaluate(env, net):
  obs = env.reset()
  reward = 0.0
  steps = 0
  behavior = []
  while True:
    obs_v = torch.FloatTensor([obs])
    action = net(obs_v)
    action = action.data.cpu().numpy()
    action = np.clip(action,
                     env.action_space.low[0],
                     env.action_space.high[0])[0].astype(np.float32)
    obs, r, done, _ = env.step(action)
    reward += r
    steps += 1
    behavior.append(obs)
    if done:
      break
  #behavior = behavior[::behavior_sample_rate] # works for pendulum
  behavior = behavior[-behavior_tail_size] # works for mountain car
  #behavior = behavior[0:-behavior_tail_size:behavior_sample_rate] + behavior[-behavior_tail_size:]
  behavior = np.append(np.array(behavior).reshape(-1), [reward])
  return behavior, steps

def make_env():
  return make_pendulum_env()
  #return make_mountain_car_env()

if __name__ == '__main__':
  mp.set_start_method('spawn')
  writer = SummaryWriter()

  input_queues = []
  output_queue = mp.Queue(maxsize=workers_count)
  workers = []

  for _ in range(workers_count):
    input_queue = mp.Queue(maxsize=1)
    input_queues.append(input_queue)
    w = mp.Process(target=worker_func,
                   args=(input_queue, output_queue,
                         noise_std, make_env, evaluate))
    w.start()
    workers.append(w)
    seeds = [(np.random.randint(max_seed),) for _ in range(seeds_per_worker)]
    input_queue.put(seeds)
  
  gen_idx = 0
  elite = None
  max_reward_mean = None
  
  behavior_archive = []
  seeds_archive = []

  best_fitness_score = None
  best_agent_seeds = None
  best_fitness_l = []

  num_episodes = 0
  
  while True:
    t_start = time.time()    
    batch_steps = 0
    population = []
    
    while len(population) < seeds_per_worker * workers_count:
      out_item = output_queue.get()
      population.append((out_item.seeds, out_item.metric))    
      batch_steps += out_item.steps

    num_episodes += seeds_per_worker * workers_count
      
    if elite is not None:
      population.append(elite)

    population_behaviors = np.array([p[1] for p in population]) # with the reward
    population_behaviors[:, -1] *= reward_weight # scale down the reward
    #population_behaviors = np.array([p[1][:-1] for p in population]) # without the reward
    
    novelty_scores = []
    best_novelty = None
    best_behavior = None
    best_seed = None
    best_added_to_archive = False
    
    for k in range(len(population)):
      novelty_score_k = novelty_func(k, population_behaviors,
                                     behavior_archive, nearest_neighbors)
      novelty_scores.append(novelty_score_k)

      fitness_score = population[k][1][-1]
      if best_fitness_score is None or best_fitness_score < fitness_score:
        best_fitness_score = fitness_score
        best_agent_seeds = population[k][0]
      
      if best_novelty is None or best_novelty < novelty_score_k:
        best_novelty = novelty_score_k
        best_behavior = population_behaviors[k]
        best_seed = population[k][0]
        best_added_to_archive = False
        
      if np.random.rand() < add_to_archive_prob:
        behavior_archive.append(population_behaviors[k])
        seeds_archive.append(population[k][0])
        if best_novelty == novelty_score_k:
          best_added_to_archive = True
          
    if best_added_to_archive == False:
      behavior_archive.append(best_behavior)
      seeds_archive.append(best_seed)
          
    behavior_archive = behavior_archive[-archive_size:]
    seeds_archive = seeds_archive[-archive_size:]
    
    novelty_scores = np.array(novelty_scores)
    sorted_idxs = np.argsort(-novelty_scores)
    
    top_population = []
    for idx in sorted_idxs[:parents_count]:
      top_population.append(population[idx])
        
    novelties = novelty_scores[sorted_idxs[:parents_count]]
    novelties_mean = np.mean(novelties)
    novelties_min = np.min(novelties)
    novelties_max = np.max(novelties)
    novelties_std = np.std(novelties)
    
    writer.add_scalar('novelties_mean', novelties_mean, gen_idx)
    writer.add_scalar('novelties_std', novelties_std, gen_idx)
    writer.add_scalar('novelties_max', novelties_max, gen_idx)
    writer.add_scalar('batch_steps', batch_steps, gen_idx)
    writer.add_scalar('gen_seconds', time.time() - t_start, gen_idx)
    speed = batch_steps / (time.time() - t_start)
    writer.add_scalar('speed', speed, gen_idx)
    
    print('%d, %d: mean=%.2f, min=%.2f, max=%.2f, std=%.2f, speed=%.2f f/s, max_fitness=%.2f' %
          (gen_idx, num_episodes, novelties_mean, novelties_min,
           novelties_max, novelties_std, speed, best_fitness_score))
    best_fitness_l.append(best_fitness_score)
       
    elite = top_population[0]

    if interrupted:
      env = make_env()
      net = build_net(env, best_agent_seeds, noise_std)
      torch.save(net.state_dict(), 'best-agent.data')
      print('Stop!')
      break
    
    for worker_queue in input_queues:
      seeds = []
      for _ in range(seeds_per_worker):
        parent = np.random.randint(parents_count)
        next_seed = np.random.randint(max_seed)
        seeds.append(tuple(list(top_population[parent][0]) + [next_seed]))
      worker_queue.put(seeds)
    gen_idx += 1
  
  for worker_queue in input_queues:
    worker_queue.put(None)
    
  for worker in workers:
    worker.join()

  #np.save("best_fitness_novelty.npy", best_fitness_l)
  writer.close()
  
