# Neuroevolution - Novelty Search

A genetic algorithm that uses a novelty function for selection of candidate neural networks in the population. The code base is a modification of the solution found in the book "Deep Reinforcement Learning Hands-On" by Maxim Lapan. The modified/extended code uses a novelty metric to score individuals in the population in place of a classic fitness score. Tested on two OpenAI gym reinforcement learning environments: Pendulum and Mountain Car. Both environments had to be modified to always start from the same initial conditions so that the comparison of novelty scores between candidates was meaningful.

# References
* Maxim Lapan. (2018) Deep Reinforcement Learning Hands-On. Packt Publishing
* Felipe Petroski Such & Vashisht Madhavan & Edoardo Conti & Joel Lehman & Kenneth O. Stanley & Jeff Clune. (2017) Deep Neuroevolution: Genetic Algorithms Are a Competitive Alternative
 for Training Deep Neural Networks for Reinforcement Learning. CoRR
* Joel Lehman & Kenneth O. Stanley. (2011) Abandoning Objectives: Evolution through the Search for Novelty Alone. Evolutionary Computation

# Implementation notes
Below are some informal diagrams that I find useful in understanding some mechanisms used in the implementation. Note: every candidate in the population is represented as a list of random generator seeds. To get the candidate neural network sum all sampled neural network weights from normal distribution each for different generator seed from the list.

![](design_note_1.jpg "Design note #1")
![](design_note_2.jpg "Design note #2")
![](design_note_3.jpg "Design note #3")
